var obsidian = require('obsidian');
console.log("scroll-dataView is load...")

function scrollViewFro(){
    let myTitle=app.workspace.activeLeaf.view.file.basename
    let target
    let frontTarget
    let tr

    
    //寻找目标
    let leafs=document.getElementsByClassName('workspace-leaf')
    let tables
    let links
    
    if(leafs.length){
        tables=leafs[0].getElementsByClassName('table-view-table')
        if(tables.length){
            links= tables[0].getElementsByClassName('internal-link')
            for(var i=0;i<links.length;i++){
                if( links[i].innerText==myTitle){
                    target=links[i]
                }
            }
        }
    }
    //找到了
    tr=target?target.parentNode.parentNode.parentNode.previousElementSibling:false
    if(tr){
        
        frontTarget=tr.getElementsByClassName('internal-link')[0]
        //跳转
        frontTarget.click()
        //a:可视区域高度 b:滚动条位置 c:当前行位置 d:当前行高度
        let box=tables[0].parentNode.parentNode.parentNode.parentNode
        let a=document.documentElement.clientHeight
        let b=box.scrollTop
        let c=tr.offsetTop
        let d=tr.offsetHeight
        if(tr&&c>b+d&&c<b+a-d){
            tr.scrollIntoView({
                behavior: 'smooth',
                block: 'center', // could be negative value
                inline: "nearest"
            })
        }else{
            tr.scrollIntoView({
                block: 'center', // could be negative value
                inline: "nearest"
            })
        }
        //滚动视图
        
        //闪烁
        var timer = null;   //定义时间器
        var i = 0; 
        clearInterval(timer); //先清空时间器
        timer = setInterval(function () {
            if(i++ % 2 ){
                tr.removeAttribute('style');
            }else{
                tr.style.backgroundColor="#f00"
            }
            if(i > 2){
                clearInterval(timer)
                tr.removeAttribute('style');
            }
        }, 250 );
        
    }
    
};
function scrollViewLat(){
    let myTitle=app.workspace.activeLeaf.view.file.basename
    let target
    let latterTarget
    let tr

    
    //寻找目标
    let leafs=document.getElementsByClassName('workspace-leaf')
    let tables
    let links
    
    if(leafs.length){
        tables=leafs[0].getElementsByClassName('table-view-table')
        if(tables.length){
            links= tables[0].getElementsByClassName('internal-link')
            for(var i=0;i<links.length;i++){
                if( links[i].innerText==myTitle){
                    target=links[i]
                }
            }
        }
    }
    //找到了
    tr=target?target.parentNode.parentNode.parentNode.nextElementSibling:false
    if(tr){
        latterTarget=tr.getElementsByClassName('internal-link')[0]
        latterTarget.click()
        
        let box=tables[0].parentNode.parentNode.parentNode.parentNode
        let a=document.documentElement.clientHeight
        let b=box.scrollTop
        let c=tr.offsetTop
        let d=tr.offsetHeight
        if(tr&&c>b+d&&c<b+a-d){
            tr.scrollIntoView({
                behavior: 'smooth',
                block: 'center', // could be negative value
                inline: "nearest"
            })
        }else{
            tr.scrollIntoView({
                block: 'center', // could be negative value
                inline: "nearest"
            })
        }
        var timer = null;   //定义时间器
        var i = 0; 
        clearInterval(timer); //先清空时间器
        timer = setInterval(function () {
            if(i++ % 2 ){
                tr.removeAttribute('style');
            }else{
                tr.style.backgroundColor="#f00"
            }
            if(i > 2){
                clearInterval(timer)
                tr.removeAttribute('style');
            }
        }, 250 );
    }
    
};
function scrollView(){
    let myTitle=app.workspace.activeLeaf.view.file.basename
    let target
    let tr

    
    //寻找目标
    let leafs=document.getElementsByClassName('workspace-leaf')
    let tables
    let links
    
    if(leafs.length){
        tables=leafs[0].getElementsByClassName('table-view-table')
        if(tables.length){
            links= tables[0].getElementsByClassName('internal-link')
            for(var i=0;i<links.length;i++){
                if( links[i].innerText==myTitle){
                    target=links[i]
                }
            }
        }
    }
    //找到了
    if(target){
        //滚动
        let tr=target.parentNode.parentNode.parentNode
        
        let box=tables[0].parentNode.parentNode.parentNode.parentNode
        let a=document.documentElement.clientHeight
        let b=box.scrollTop
        let c=tr.offsetTop
        let d=tr.offsetHeight
        if(tr&&c>b+d&&c<b+a-d){
            tr.scrollIntoView({
                behavior: 'smooth',
                block: 'center', // could be negative value
                inline: "nearest"
            })
        }else{
            tr.scrollIntoView({
                block: 'center', // could be negative value
                inline: "nearest"
            })
        }
        //闪烁
        var timer = null;   //定义时间器
        var i = 0; 
        clearInterval(timer); //先清空时间器
        timer = setInterval(function () {
            if(i++ % 2 ){
                tr.removeAttribute('style');
            }else{
                tr.style.backgroundColor="#f00"
            }
            if(i > 2){
                clearInterval(timer)
                tr.removeAttribute('style');
            }
        }, 250 );
    }
    
};
class MyPlugin extends obsidian.Plugin {
    async onload() {
        this.addCommand({
            id: 'scroFront',
            name: '前一个文件',
            checkCallback: scrollViewFro
        });
        this.addCommand({
            id: 'scro',
            name: '当前文件',
            callback: scrollView
        });
        this.addCommand({
            id: "scroLat",
            name: "后一个文件",
            checkCallback: scrollViewLat
        });
    }
}
module.exports = MyPlugin;