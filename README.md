# Obsidian-ScrollDataView

## 更新日志
### 0.0.1 版本
- 最初版本

### 0.0.2版本
- 添加了简单的更改侧边栏图标功能


![输入图片说明](IMG/Pasted%20image%2020220813004555.png)

![输入图片说明](Pasted%20image%2020220813004744.png)

图标列表：（抄的这个插件的：obsidian-customizable-sidebar）
![输入图片说明](IMG/icons.png)

## 介绍

dataview插件筛选出来的文件表格很适合停靠在侧边栏作为目录，这个插件就是为这样的目录设计，添加了三个命令
1. 在侧边栏dataview表格中查看当前笔记
2. 切换到dataview表格中本文件的上一文件
3. 切换到dataview表格中本文件的下一文件

## 使用展示
![输入图片说明](IMG/IMGGIF1.gif)
按下设置好的快捷键进行文件定位与跳转


## 使用说明

1. 使用dataview插件对文件进行筛选排序

![输入图片说明](IMG/IMGPasted%20image%2020220812152254.png)

2. 预览视图下的表格

![输入图片说明](IMG/IMGPasted%20image%2020220812150439.png)

3. 将这个笔记文件停靠固定在左侧侧边栏（一定要是左侧，并处于显示状态）

![输入图片说明](IMG/IMGPasted%20image%2020220812150730.png)

4. 为命令添加快捷键

![输入图片说明](IMG/IMGPasted%20image%2020220812151144.png)

5. (再次提醒)保持dataview目录侧边栏显示状态，可以收起，但一定要选中这个侧边栏

![输入图片说明](IMG/IMGPasted%20image%2020220812150730.png)

6. 使用设置好的快捷键进行文件跳转与定位

## 安装教程
1. 在.obsidian文件夹下新建文件夹ScrollDataView，
2. 下载Obsidian-ScrollDataView文件夹下的main.js、manifest.json、styles.css三个文件
3. 将其放在.obsidian/ScrollDataView文件夹下
4. 启动obsidian,开启插件

## 补充
如上示例，文件名以等于号+r、y、g、b结尾的笔记文件会被赋予不同的背景色
